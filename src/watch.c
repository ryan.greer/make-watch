/**
 *  @file watch.c
 * 
 *  This file contains functions used to manage directories watched by
 *  make-watch.
 */

#define _XOPEN_SOURCE 500
#define _GNU_SOURCE

/* Standard Library Headers */
#include <errno.h>
#include <ftw.h>
#include <fcntl.h>
#include <libgen.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/inotify.h>

/* Project Headers */
#include "make.h"

/* Static variables used by watch.c */
static int added, inotify_fd, inotify_flags = 0;
static size_t event_len;
static struct inotify_event * event_buf;
static int inotify_mask = IN_CLOSE_WRITE | IN_DELETE;
static const char * watch_exts[] = { ".c", ".h" };

/**
 *  Add a path to the list of subdirectories to watch. This function is passed
 *  as a function pointer to the nftw() call and is subsequently called once
 *  for each file or directory found.
 * 
 *  @param fpath    String reflecting the path to the file or directory found by
 *                  nftw()
 *  @param sb       Unused
 *  @param typeFlag Integer representing the flags that indicate the type of
 *                  fpath
 *  @param ftwbuf   Unused
 * 
 *  @return Integer 0 indicating success unless there is a memory error. Return -1
 *          if the directory is not added to the path.
*/
static int add_path(const char * fpath, const struct stat * sb, int typeflag, struct FTW * ftwbuf) {
    (void)(sb);
    (void)(ftwbuf);

    if (0 == inotify_fd)
    {
        return FTW_STOP;
    }

    if (!(typeflag & FTW_D))
    {
        return FTW_CONTINUE;
    }
    if (strncmp(basename((char *)fpath), ".", 1) == 0)
    {
        return FTW_SKIP_SUBTREE;
    }

    if (-1 == inotify_add_watch(inotify_fd, fpath, inotify_mask))
    {
        printf("Failed to add %s to watch list due to: %s\n", \
            fpath, strerror(errno));

        return FTW_STOP;
    }

    printf("Watching %s\n", fpath);
    added++;

    return FTW_CONTINUE;
}

/* Initializes the watcher */
int watch_init()
{
    event_len = sizeof(struct inotify_event) + FILENAME_MAX + 1;
    inotify_fd = inotify_init();

    if (-1 == inotify_fd)
    {
        printf("Failed to initialize the inotify due to: %s\n", \
            strerror(errno));

        return -1;
    }

    if ((event_buf = (struct inotify_event*) calloc(1, event_len)) == NULL)
    {
        printf("Failed to allocate memory to read inotify events due to: %s\n", \
            strerror(errno));

        exit(-1);
    }

    return 0;
}

/* Adds a directory and its subdirectories to watch */
int watch_directory(const char * directory)
{
    if (NULL == directory)
    {
        return -1;
    }

    added = 0;

    nftw(directory, &add_path, 10, FTW_ACTIONRETVAL);

    return added;
}

void watch_start(const char * target)
{
    bool valid_ext;

    if (make_cmd((char *) target) == 0)
    {
        while (read(inotify_fd, event_buf, event_len) != -1)
        {
            valid_ext = false;

            for (int ext_idx = 0; ext_idx < 2; ext_idx++)
            {
                int ext_len = strlen(watch_exts[ext_idx]);
                char* ext = (event_buf->name + (strlen(event_buf->name) - ext_len));

                if (strncmp(ext, watch_exts[ext_idx], ext_len) == 0)
                {
                    valid_ext = true;
                }
            }

            if (!valid_ext)
            {
                continue;
            }

            if (fcntl(inotify_fd, F_SETFL, O_NONBLOCK) == -1) 
            {
                break;
            }

            for (int num_events = 10; num_events > 0; num_events--)
            {
                read(inotify_fd, event_buf, event_len);
            }

            inotify_flags = fcntl(inotify_fd, F_GETFL);

            if (fcntl(inotify_fd, F_SETFL, inotify_flags & ~O_NONBLOCK) == -1) {
                printf("Failed to set the inotify fd to non-blocking due to: %s\n", \
                    strerror(errno));
                    
                break;
            }

            if (make_cmd((char *) target) != 0)
            {
                printf("Failed to spawn the shell for make due to: %s\n", \
                    strerror(errno));

                break;
            }
        }
    }
}

/* Destroys and cleans up the watcher */
void watch_destroy()
{
    if (NULL != event_buf)
    {
        free(event_buf);
    }
}
