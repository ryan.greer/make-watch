/**
 *  @file main.c
 * 
 *  This program watches common C source directories for changes to .c
 *  and .h files, then executes the specified make target upon changes.
 */

/* Standard Library Headers */
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Project Headers */
#include "watch.h"

/** String representing the current working directory */
char cwd[1024] = {0};

/** Global event buffer so it can be freed on SIG INT */
struct inotify_event * event_buf;

/** Print the help menu. */
void usage()
{
    printf(
        "Usage: make-watch [ OPTIONS ... ] [ TARGET ]\n"
        "\n"
        "Continuous Make Runner for C Projects\n"
        "\n"
        "Options:\n"
        "    -h, --help           Show the help menu.\n"
        "    -d DIR, --dir DIR    Specify a directory to watch. May be used\n"
        "                         multiple times. Specifying at least one\n"
        "                         directory means that only specified\n"
        "                         directories will be watched.\n"
        "\n"
        "Positional Arguments:\n"
        "    TARGET    Make target to run on changes. Default is none.\n"
        "\n"
        "README available at: <https://gitlab.com/ryan.greer/make-watch>\n"
    );
}

/**
 *  Called any time the process receives the INT signal. Frees the global
 *  event_buf memory and exits cleanly.
 * 
 *  @param sig_num Interger representing the signal.
*/
void sigint_handler(const int sig_num) {
    signal(SIGINT, sigint_handler);

    if (SIGINT == sig_num) {
        free(event_buf);
        exit(0);
    }
}

/**
 *  Main function called when the program is executed.
 * 
 *  @param argc Integer indicating the number of arguments.
 *  @param argv Array of strings representing the arguments.
 * 
 *  @return Interger indicating the status of the program at exit.
 */
int main(int argc, char * argv[])
{
    char * target = NULL;
    bool watch_cwd = true;

    if (watch_init() == -1)
    {
        printf("Failed to initialize the watch list.\n");

        exit(-1);
    }

    for (int opt_idx = 1; opt_idx < argc; opt_idx++)
    {
        if ((strcmp("-h", argv[opt_idx]) == 0) || \
            (strcmp("--help", argv[opt_idx]) == 0))
        {
            usage();
            exit(0);
        }
        else if ((strcmp("-d", argv[opt_idx]) == 0) || \
            (strcmp("--dir", argv[opt_idx]) == 0))
        {
            watch_directory(argv[opt_idx + 1]);
            opt_idx++;
            watch_cwd = false;
        }
        else
        {
            target = argv[opt_idx];
        }
    }

    signal(SIGINT, sigint_handler);

    if (watch_cwd)
    {
        if (getcwd(cwd, 1024 - 1) == NULL)
        {
            printf("Failed to get the pwd due to: %s\n", strerror(errno));

            exit(-1);
        }

        if (watch_directory(cwd) <= 0)
        {
            printf("Failed to start watching pwd.\n");

            exit(-1);
        }
    }

    watch_start(target);

    printf("Stopped watching due to: %s\n", strerror(errno));

    return -1;
}
